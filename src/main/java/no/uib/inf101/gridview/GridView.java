package no.uib.inf101.gridview;


import javax.swing.JPanel;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel{

  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  
  IColorGrid colorGrid;
  public GridView(IColorGrid colorGrid ){
    this.setPreferredSize(new Dimension(400, 300));
    this.colorGrid=colorGrid;
  }
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    this.drawGrid(g2);
  }

   private void drawGrid(Graphics2D g2d){
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    g2d.setColor(MARGINCOLOR);
    Rectangle2D rectangle2d = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
    g2d.fill(rectangle2d);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(rectangle2d, colorGrid, OUTERMARGIN);
    drawCells(g2d, colorGrid, converter );

   }

   private static void drawCells(Graphics2D g2, CellColorCollection ccc, CellPositionToPixelConverter converter){
    for(CellColor cell : ccc.getCells()){
      g2.setColor(cell.color()==null ? Color.DARK_GRAY : cell.color());
      g2.fill(converter.getBoundsForCell(cell.cellPosition()));
    }

    
   }

   
}
