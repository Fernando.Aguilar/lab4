package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  int rows;
  int colums;
  Color[][] grid;
  

  public ColorGrid(int rows, int colums){
    this.colums = colums;
    this.rows = rows;
    this.grid = new Color [rows][colums];
    
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.colums;
  }

  @Override
  public List<CellColor> getCells() {
    ArrayList<CellColor> lista = new ArrayList<>();
    
    for(int row=0; row<this.grid.length; row++){
      for(int col=0; col<this.grid[row].length; col++){
        lista.add(new CellColor(new CellPosition(row, col), this.grid[row][col]));
        }
    }
    return lista;
  }

    
  public boolean isOutOfBounds(CellPosition pos){
    return (pos.row() < 0 || pos.row() >= this.grid.length || pos.col() < 0 || pos.col() >= this.grid[0].length); 
      
  }

  @Override
  public Color get(CellPosition pos) {

    if (isOutOfBounds(pos)) {
      throw new IndexOutOfBoundsException("the position is out of bounds");
    }
          
      return this.grid[pos.row()][pos.col()];
        
  }

  @Override
  public void set(CellPosition pos, Color color) {
    if (isOutOfBounds(pos)) {
      throw new IndexOutOfBoundsException("the position is out of bounds");
    }
   
     this.grid[pos.row()][pos.col()] = color;
  }
}

